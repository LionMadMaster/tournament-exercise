<?php
$config = [
    'aliases' => [
        'axe'         => \Tournament\Equipment\Weapon\OneHahdedAxe::class,
        'great_sword' => \Tournament\Equipment\Weapon\GreatSword::class,
        'sword'       => \Tournament\Equipment\Weapon\OneHahdedSword::class,

        'armor'   => \Tournament\Equipment\Armor\Armor::class,
        'buckler' => \Tournament\Equipment\Armor\Buckler::class,
    ]
];