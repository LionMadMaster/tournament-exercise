<?php
namespace Tournament;

use phemto\Phemto;
use Tournament\Duel\Duel;
use Tournament\Equipment\EquipmentCommon;
use Tournament\Equipment\EquipmentFactory;

/**
 * Class FighterCommon
 *
 * The main class
 * Used to describe Fighter and, in the absence of the main container, initialization environment
 *
 * @package Tournament
 */
abstract class FighterCommon implements FighterInterface
{
    /**
     * @var Phemto
     * It used to implement DI pattern
     * Bad? Yes...
     */
    protected $injector;
    /**
     * @var integer Initial and processable Hit Points for instance of the Fighter
     */
    protected $hit_points;
    /**
     * @var bool|string Initial and processable ALIAS Weapon for instance of the Fighter
     */
    protected $weapon = false;
    /**
     * @var bool|string|array Initial and processable ALIAS(ES) Armors for instance of the Fighter
     */
    protected $armor = false;
    /**
     * @var bool|string Special value for mutation magic
     */
    protected $mutate = false;

    /**
     * @var array Current equipments for instance of the Fighter
     */
    protected $equipments = [];

    /**
     * FighterCommon constructor.
     * Initial place for all code
     * @param bool|string $mutate
     */
    public function __construct($mutate = false)
    {
        $this->injector = new Phemto();
        $this->equip($this->weapon);
        $this->equip($this->armor);

        if ($mutate) {
            $mutate       = ucwords(str_replace(['-', '_'], ' ', $mutate));
            $this->mutate = str_replace(' ', '', $mutate);;
        }
    }

    /**
     * Show or set Hit Points for Fighter
     * @param bool $points
     * @param bool $relative if TRUE -- append $points to current Hit Points value
     * @return int Hit Points
     */
    public function hitPoints($points = false, $relative = false)
    {
        if ($points) {
            if ($relative) {
                $this->hit_points = $this->hit_points + $points;
            } else {
                $this->hit_points = $points;
            }
        }
        if ($this->hit_points < 0) {
            $this->hit_points = 0;
        }
        return $this->hit_points;
    }

    /**
     * Let's FIGHT!
     * @param FighterInterface $aim
     */
    public function engage(FighterInterface $aim)
    {
        /**
         * @var Duel $duel
         */
        $duel = $this->injector->create(Duel::class);
        $duel->setFighters($this, $aim);
        $duel->fight();
    }

    /**
     * Set Equipment by alias(es) from config/equipments.php
     * @param string|array $equip_value alias or array of aliases of armor or weapons
     * @return $this
     * @throws \Exception
     */
    public function equip($equip_value)
    {
        if (!is_array($equip_value)) {
            $equip_value = [$equip_value];
        }
        foreach ($equip_value as $equip_alias) {
            $this->setEquip($equip_alias);
        }

        return $this;
    }

    /**
     * Get all current Equipments
     * @return array
     */
    public function getEquipments()
    {
        return $this->equipments;
    }

    /**
     * @return mixed
     */
    public function getMutate()
    {
        return $this->mutate;
    }

    /**
     * Set Equipment by alias
     * @param $equip_alias
     * @throws \Exception
     */
    private function setEquip($equip_alias)
    {
        if ($equip_alias) {
            $equip_obj = EquipmentFactory::getEquipmentInstance($equip_alias);
            if ($equip_obj->getType() == EquipmentCommon::EQUIPMENT_TYPE_WEAPON) {
                $this->equipments[$equip_obj->getType()] = $equip_obj;
            } else {
                $this->equipments[$equip_obj->getType()][$equip_obj->getAlias()] = $equip_obj;
            }
        }
    }

}