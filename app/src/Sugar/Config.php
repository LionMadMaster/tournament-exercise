<?php
namespace Tournament\Sugar;

/**
 * Class Config
 *
 * Helper for work with config files
 * configs place in /app/src/configs folder
 *
 *
 * @package Tournament\Sugar
 */
class Config
{
    /**
     * Some like session cash for limiting filesystem operations
     * @var array
     */
    private static $configs = [];

    /**
     * Get config item by doc-separated key string with fallback
     *
     * @param      $key
     * @param bool $default
     * @return bool|mixed
     */
    public static function get($key, $default = false)
    {
        $keys = explode('.', $key);
        $file = array_shift($keys);

        $array = self::readFile($file);

        return Arr::get($array, $keys, $default);
    }

    /**
     * Init config file|Read data for cash
     *
     * @param $file
     * @return mixed
     */
    private static function readFile($file)
    {
        if (!isset(self::$configs[$file])) {
            include __DIR__ . '/../config/' . $file . '.php';
            self::$configs[$file] = isset($config) ? $config : [];
        }
        return self::$configs[$file];
    }
}