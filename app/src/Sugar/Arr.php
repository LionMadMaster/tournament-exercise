<?php
namespace Tournament\Sugar;

/**
 * Class Arr
 *
 * Helper for work with arrays
 *
 * @package Tournament\Sugar
 */
class Arr
{
    /**
     * Get array item by doc-separated key string with fallback
     *
     * @param array        $array
     * @param array|string $keys
     * @param mixed        $default
     * @return mixed
     */
    public static function get($array, $keys, $default = false)
    {
        if (!is_array($keys)) {
            $keys = explode('.', $keys);
        }

        foreach ($keys as $segment) {
            if (is_array($array) && isset($array[$segment])) {
                $array = $array[$segment];
            } else {
                return $default;
            }
        }

        return $array;
    }
}