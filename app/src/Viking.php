<?php
namespace Tournament;

/**
 * A Viking has 120 hit points and use a 1 hand axe
 *
 * Class Viking
 * @package Tournament
 */
class Viking extends FighterCommon
{
    /**
     * @var int
     */
    protected $hit_points = 120;
    /**
     * @var bool|string
     */
    protected $weapon = 'axe';
    /**
     * @var bool|string
     */
    protected $armor = false;

}
