<?php
namespace Tournament\Equipment\Armor;


use Tournament\Duel\DuelLog;
use Tournament\Equipment\EquipmentInterface;

/**
 * reduce all received damages by 3 & reduce delivered damages by one
 *
 * Class Armor
 * @package Tournament\Equipment\Armor
 * @see     EquipmentInterface
 */
class Armor extends ArmorCommon
{
    /**
     * @var int reduce all received damages by 3
     */
    protected $blocked_damage = 3;

    /**
     *  reduce delivered damages by one
     *
     * @param DuelLog $log
     * @return int
     */
    public function getDamage(DuelLog $log)
    {
        return -1;
    }

}