<?php
namespace Tournament\Equipment\Armor;


use Tournament\Equipment\EquipmentCommon;

/**
 * Class ArmorCommon
 * @package Tournament\Equipment\Armor
 */
abstract class ArmorCommon extends EquipmentCommon
{
    /**
     * @var string
     */
    public $type = self::EQUIPMENT_TYPE_ARMOR;
}