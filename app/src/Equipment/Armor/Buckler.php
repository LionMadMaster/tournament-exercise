<?php
namespace Tournament\Equipment\Armor;


use Tournament\Duel\DuelLog;

/**
 * a buckler cancel all the damages of a blow one time out of two
 * a buckler is destroyed after blocking 3 blow from an axe
 *
 * Class Buckler
 * @package Tournament\Equipment\Armor
 */
class Buckler extends ArmorCommon
{
    /**
     * @param string      $weapon
     * @param boolean|int $damage
     * @param DuelLog     $log
     * @return int resulted damage
     */
    public function getBlockedDamage($weapon, $damage, DuelLog $log)
    {
        if ($this->destroyed) {
            return 0;
        }

        $defends_count = $log->getDefendsCount(
            function ($item) {
                return !$item['skipped'];
            }
        );

        if (($defends_count) % 2 == 0) {
            return $damage;
        }

        if ($log->getDefendsCount(
                function ($item) {
                    if ($item['weapon'] == 'axe' && $item['damage'] == 0) {
                        return true;
                    }
                    return false;
                }
            ) >= 3
        ) {
            $this->setDestroyed(true);
        }

        return 0;
    }

}