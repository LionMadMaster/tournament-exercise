<?php

namespace Tournament\Equipment;


use phemto\Phemto;
use Tournament\Sugar\Config;

/**
 * Sugar class for generate Equipment instance by alias
 * Aliases places in /app/src/config/equipments.php
 *
 * Class EquipmentFactory
 * @package Tournament\Equipment
 */
class EquipmentFactory
{
    /**
     * Return Equipment instance by alias
     *
     * @param string $alias
     * @return EquipmentInterface
     * @throws \Exception
     */
    public static function getEquipmentInstance($alias)
    {
        $class_name = Config::get('equipments.aliases.' . $alias);

        if (!$class_name) {
            throw new \Exception('There is no such equipment: ' . $alias);
        }

        /**
         * Phemto using for DI
         */
        $injector = new Phemto();
        $injector->willUse($class_name);
        /**
         * @var EquipmentInterface $equipment_obj
         */
        $equipment_obj = $injector->create(EquipmentInterface::class);
        $equipment_obj->setAlias($alias); // !!!
        return $equipment_obj;
    }
}