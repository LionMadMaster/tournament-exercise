<?php
namespace Tournament\Equipment\Weapon;


use Tournament\Duel\DuelLog;

/**
 *  Great Sword is a two handed sword deliver 12 damages, but can attack only 2 every 3)(attack ; attack ; no attack)
 *
 * Class GreatSword
 * @package Tournament\Equipment\Weapon
 */
class GreatSword extends WeaponCommon
{
    /**
     * deliver 12 damages
     *
     * @var int
     */
    public $damage = 12;

    /**
     * can attack only 2 every 3)(attack ; attack ; no attack)
     *
     * @param DuelLog $log
     * @return bool|int
     */
    public function getDamage(DuelLog $log)
    {
        if (!$this->destroyed) {
            if (!(($log->getAttacksCount() + 1) % 3) == 0) {
                return $this->damage;
            } else {
                return false;
            }
        }

        return 0;
    }
}