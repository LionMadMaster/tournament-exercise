<?php
namespace Tournament\Equipment\Weapon;


use Tournament\Equipment\EquipmentCommon;

/**
 * Class WeaponCommon
 * @package Tournament\Equipment\Weapon
 */
abstract class WeaponCommon extends EquipmentCommon
{
    /**
     * @var string
     */
    public $type = self::EQUIPMENT_TYPE_WEAPON;

}