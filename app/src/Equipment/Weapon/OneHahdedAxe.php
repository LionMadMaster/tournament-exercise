<?php
namespace Tournament\Equipment\Weapon;


/**
 * 1 hand axe that does 6 dmg
 *
 * Class OneHahdedAxe
 * @package Tournament\Equipment\Weapon
 */
class OneHahdedAxe extends WeaponCommon
{
    /**
     * does 6 dmg
     *
     * @var int
     */
    public $damage = 6;
}