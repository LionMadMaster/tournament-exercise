<?php
namespace Tournament\Equipment\Weapon;


/**
 * 1 hand sword that does 5 dmg
 *
 * Class OneHahdedSword
 * @package Tournament\Equipment\Weapon
 */
class OneHahdedSword extends WeaponCommon
{
    /**
     * does 5 dmg
     *
     * @var int
     */
    public $damage = 5;
}