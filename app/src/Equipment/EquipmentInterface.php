<?php
/**
 * Created by PhpStorm.
 * User: snovidov
 * Date: 13.09.16
 * Time: 16:48
 */

namespace Tournament\Equipment;


use Tournament\Duel\DuelLog;

/**
 * Interface EquipmentInterface
 * @package Tournament\Equipment
 */
interface EquipmentInterface
{
    /**
     * @return mixed
     */
    public function getType();

    /**
     * @return mixed
     */
    public function getAlias();

    /**
     * @param $alias
     * @return mixed
     */
    public function setAlias($alias);

    /**
     * @return mixed
     */
    public function isDestroyed();

    /**
     * @param $destroyed
     * @return mixed
     */
    public function setDestroyed($destroyed);

    /**
     * @param string      $weapon
     * @param int|boolean $damage
     * @param DuelLog     $log
     * @return int received damage
     */
    public function getBlockedDamage($weapon, $damage, DuelLog $log);

    /**
     * @param DuelLog $log
     * @return int damage
     */
    public function getDamage(DuelLog $log);
}