<?php
namespace Tournament\Equipment;


use Tournament\Duel\DuelLog;

/**
 * Class EquipmentCommon
 * @package Tournament\Equipment
 */
abstract class EquipmentCommon implements EquipmentInterface
{

    const EQUIPMENT_TYPE_WEAPON = 'weapon';
    const EQUIPMENT_TYPE_ARMOR = 'armor';

    /**
     * @var string set in instance
     */
    protected $type;

    /**
     * @var string set in fabric
     * @see EquipmentFactory::getEquipmentInstance()
     */
    protected $alias;

    /**
     * @var bool
     */
    protected $destroyed = false;

    /**
     * @var int
     */
    protected $damage = 0;
    /**
     * @var int
     */
    protected $blocked_damage = 0;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param mixed $alias
     * @return mixed
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isDestroyed()
    {
        return $this->destroyed;
    }

    /**
     * @param boolean $destroyed
     * @return mixed
     */
    public function setDestroyed($destroyed)
    {
        $this->destroyed = $destroyed;
        return $this;
    }


    /**
     * Default blocked damage calculate
     *
     * @param string      $weapon
     * @param int|boolean $damage
     * @param DuelLog     $log
     * @return int received damage
     */
    public function getBlockedDamage($weapon, $damage, DuelLog $log)
    {
        if (!$this->destroyed) {
            return $this->blocked_damage;
        }

        return 0;
    }

    /**
     * Default damage calculate
     *
     * @param DuelLog $log
     * @return int damage
     */
    public function getDamage(DuelLog $log)
    {
        if (!$this->destroyed) {
            return $this->damage;
        }

        return 0;
    }
}