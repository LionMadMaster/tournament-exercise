<?php
namespace Tournament;

use Tournament\Duel\DuelLog;

/**
 * an Highlander as 150 hit points and fight with a Great Sword
 *
 * Class Highlander
 * @see     FighterCommon
 * @package Tournament
 */
class Highlander extends FighterCommon
{
    /**
     * @var int
     */
    protected $hit_points = 150;
    /**
     * @var bool|string
     */
    protected $weapon = 'great_sword';
    /**
     * @var bool|string
     */
    protected $armor = false;

    /**
     * Magic method for Veteran mutation
     * veteran Highlander goes Berserk once his hit points are under 30% of his initial total
     * once Berserk, he doubles his damages
     *
     * @param int     $damage Normal generated damage
     * @param DuelLog $log
     * @return int Append damage
     * @see DuelBlow::getMutationDamage
     */
    public function getVeteranDamage($damage, DuelLog $log)
    {
        if ((round($this->hitPoints() / 150, 2) < 0.30)) {
            return $damage;
        }
        return 0;
    }
}
