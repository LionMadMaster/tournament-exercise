<?php
namespace Tournament;


/**
 * Interface FighterInterface
 * @package Tournament
 */
interface FighterInterface
{
    /**
     * Show or set Hit Points for Fighter
     * @param bool $points
     * @param bool $relative if TRUE -- append $points to current Hit Points value
     * @return int Hit Points
     */
    public function hitPoints($points = false, $relative = false);

    /**
     * Set Equipment by alias(es) from config/equipments.php
     * @param string|array $equip_value alias or array of aliases of armor or weapons
     * @return $this
     * @throws \Exception
     */
    public function equip($equip_value);

    /**
     * Let's FIGHT
     * @param FighterInterface $aim Adversary
     */
    public function engage(FighterInterface $aim);

    /**
     * Get all current Equipments
     * @return mixed
     */
    public function getEquipments();

    /**
     * @return mixed
     */
    public function getMutate();
}