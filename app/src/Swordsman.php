<?php
namespace Tournament;

use Tournament\Duel\DuelLog;

/**
 * A Swordsman has 100 hit points and use a 1 hand sword
 *
 * Class Swordsman
 * @package Tournament
 */
class Swordsman extends FighterCommon
{
    /**
     * @var int
     */
    protected $hit_points = 100;
    /**
     * @var bool|string
     */
    protected $weapon = 'sword';
    /**
     * @var bool|string
     */
    protected $armor = false;


    /**
     * Magic method for Vicious mutation
     *
     * a vicious Swordsman is a Swordsman that put poison on his weapon.
     * poison add 20 damages on two first blows
     *
     * @param int     $damage Normal generated damage
     * @param DuelLog $log
     * @return int Append damage
     * @see DuelBlow::getMutationDamage
     */
    public function getViciousDamage($damage, DuelLog $log)
    {
        if ($log->getAttacksCount() < 2) {
            return 20;
        }
        return 0;
    }
}
