<?php
namespace Tournament\Duel;


use Tournament\Equipment\EquipmentCommon;
use Tournament\Equipment\EquipmentInterface;
use Tournament\FighterInterface;
use Tournament\Sugar\Arr;

/**
 * The logic of calculation of received and delivered damages depending on the conditions and the previous steps
 *
 * Class DuelBlow
 * @package Tournament\Duel
 */
class DuelBlow
{
    /**
     * @var FighterInterface
     */
    protected $fighter;

    /**
     * @var DuelLog previous steps for current Fighter
     */
    protected $log;

    /**
     * DuelBlow constructor.
     * @param DuelLog $duelLog
     */
    public function __construct(DuelLog $duelLog)
    {
        $this->log = $duelLog;
    }


    /**
     * @param FighterInterface $fighter
     */
    public function setFighter(
        FighterInterface $fighter
    ) {
        $this->fighter = $fighter;
    }


    /**
     * @return integer
     * @see FighterInterface::hitPoints()
     */
    public function hitPoints()
    {
        return $this->fighter->hitPoints();
    }


    /**
     * Calculate delivered damages
     * @return array
     */
    public function getDamage()
    {
        $weapon = Arr::get($this->fighter->getEquipments(), EquipmentCommon::EQUIPMENT_TYPE_WEAPON);
        if (!$weapon) {
            return $this->damageArray(false, 0);
        }

        $damage = $weapon->getDamage($this->log);
        if ($damage !== false) {
            $damage += $this->getAppendDamage($damage);
        }
        return $this->damageArray($weapon->getAlias(), $damage);
    }

    /**
     * @param string      $weapon
     * @param boolean|int $damage calculated damage
     * @return array
     */
    private function damageArray($weapon, $damage)
    {
        $this->log->setAttack($weapon, $damage);
        return [
            'weapon' => $weapon,
            'damage' => $damage,
        ];
    }

    /**
     * @param int $damage current calculated damage
     * @return int append damage
     */
    private function getAppendDamage($damage)
    {
        $append_damage = 0;
        $armors        = Arr::get($this->fighter->getEquipments(), EquipmentCommon::EQUIPMENT_TYPE_ARMOR, []);
        foreach ($armors as $armor) {
            /**
             * @var EquipmentInterface $armor
             */
            $append_damage += $armor->getDamage($this->log);
        }

        if ($this->fighter->getMutate()) {
            $append_damage += $this->getMutationDamage($damage);
        }

        return $append_damage;
    }

    /**
     * Calculate received damages
     *
     * @param string      $weapon
     * @param boolean|int $damage
     */
    public function setDamage($weapon, $damage)
    {
        if ($damage !== false) { // if $damage === false — attack skipped
            $blocked_damage = 0;
            $armors         = Arr::get($this->fighter->getEquipments(), EquipmentCommon::EQUIPMENT_TYPE_ARMOR, []);
            foreach ($armors as $armor) {
                /**
                 * @var EquipmentInterface $armor
                 */
                $blocked_damage += $armor->getBlockedDamage($weapon, $damage, $this->log);
            }

            $blocked_damage += $this->getAppendBlockedDamage($weapon, $damage);

            $damage -= $blocked_damage;
            if ($damage < 0) {
                $damage = 0;
            }
        }

        $this->log->setDefend($weapon, $damage);
        $this->fighter->hitPoints(-$damage, true);
    }

    /**
     * @param string $weapon
     * @param int    $damage current calculated damage
     * @return int append blocked damage
     */
    private function getAppendBlockedDamage($weapon, $damage)
    {
        $append_blocked_damage = 0;

        // more logic here

        if ($this->fighter->getMutate()) {
            $append_blocked_damage += $this->getMutationBlockedDamage($weapon, $damage);
        }
        return $append_blocked_damage;
    }

    /**
     * Call magic method for Fighters mutations if its present
     *
     * @param int $damage current calculated damage
     * @return int append blocked damage
     */
    private function getMutationDamage($damage)
    {
        $method_name = 'get' . $this->fighter->getMutate() . 'Damage';
        if (method_exists($this->fighter, $method_name)) {
            return $this->fighter->$method_name($damage, $this->log);
        }
        return 0;
    }

    /**
     *  Call magic method for Fighters mutations if its present
     *
     * @param $weapon
     * @param $damage
     * @return int
     */
    private function getMutationBlockedDamage($weapon, $damage)
    {
        $method_name = 'get' . $this->fighter->getMutate() . 'BlockedDamage';
        if (method_exists($this->fighter, $method_name)) {
            return $this->fighter->$method_name($weapon, $damage, $this->log);
        }
        return 0;
    }
}