<?php
namespace Tournament\Duel;


/**
 * Helper class for store fight steps for selected fighter
 *
 * Class DuelLog
 * @package Tournament\Duel
 */
class DuelLog
{
    /**
     * @var array
     */
    private $attacks_log = [];
    /**
     * @var array
     */
    private $defends_log = [];

    /**
     * Set attack step
     *
     * @param string      $weapon
     * @param boolean|int $damage
     */
    public function setAttack($weapon, $damage)
    {
        $this->attacks_log[] = [
            'weapon'  => $weapon,
            'damage'  => (int)$damage,
            'skipped' => ($damage === false),
        ];
    }

    /**
     * Set defend step
     *
     * @param string      $weapon
     * @param boolean|int $damage
     */
    public function setDefend($weapon, $damage)
    {
        $this->defends_log[] = [
            'weapon'  => $weapon,
            'damage'  => (int)$damage,
            'skipped' => ($damage === false)
        ];
    }

    /**
     * Return count of attacks
     *
     * @param bool|callable $callback [optional] Callback function as in array_filter
     * @link http://php.net/manual/en/function.array-filter.php
     * @return mixed
     */
    public function getAttacksCount($callback = false)
    {
        return $this->getCount($this->attacks_log, $callback);
    }

    /**
     * Return count of defends
     *
     * @param bool|callable $callback [optional] Callback function as in array_filter
     * @link http://php.net/manual/en/function.array-filter.php
     * @return mixed
     */
    public function getDefendsCount($callback = false)
    {
        return $this->getCount($this->defends_log, $callback);
    }

    /**
     * @param               $array
     * @param bool|callable $callback [optional] Callback function as in array_filter
     * @link http://php.net/manual/en/function.array-filter.php
     * @return mixed
     */
    private function getCount($array, $callback = false)
    {
        if ($callback) {
            $filtered_array = array_filter(
                $array,
                $callback
            );
            return count($filtered_array);
        } else {
            return count($array);
        }
    }
}