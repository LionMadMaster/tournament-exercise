<?php

namespace Tournament\Duel;


use phemto\Phemto;
use Tournament\FighterInterface;

/**
 * Main Fight logic here
 * Blow exchange are sequential, A engage B means that A will give the first blow, then B will respond and continue till the death
 *
 * Class Duel
 * @package Tournament\Duel
 */
class Duel
{
    /**
     * @var Phemto DI implementation
     */
    protected $injector;
    /**
     * @var DuelBlow handler for attacking Fighter in current time
     */
    private $attack;
    /**
     * @var DuelBlow handler for defending Fighter in current time
     */
    private $defend;

    /**
     * Duel constructor.
     * @param Phemto $injector
     */
    public function __construct(Phemto $injector)
    {
        $this->injector = $injector;
    }


    /**
     * @param FighterInterface $attack the first hit fighter
     * @param FighterInterface $defend
     */
    public function setFighters(
        FighterInterface $attack,
        FighterInterface $defend
    ) {
        $this->attack = $this->injector->create(DuelBlow::class);
        $this->defend = $this->injector->create(DuelBlow::class);

        $this->attack->setFighter($attack);
        $this->defend->setFighter($defend);

    }

    /**
     * Main Fight cycle «until the death separates us»
     *
     * @throws \Exception
     */
    public function fight()
    {
        if (!$this->attack || !$this->defend) {
            throw new \Exception('Fighters must be set');
        }
        while ($this->attack->hitPoints() > 0 && $this->defend->hitPoints() > 0) {
            $this->blow();
        }
    }

    /**
     * Fight engage
     */
    private function blow()
    {
        $blow = $this->attack->getDamage();
        $this->defend->setDamage($blow['weapon'], $blow['damage']);
        $this->rotate();
    }

    /**
     * change order
     */
    private function rotate()
    {
        $tmp          = $this->attack;
        $this->attack = $this->defend;
        $this->defend = $tmp;
    }
}